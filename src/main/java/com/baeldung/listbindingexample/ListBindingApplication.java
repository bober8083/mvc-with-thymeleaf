package com.baeldung.listbindingexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

// main application! for launching
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class ListBindingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ListBindingApplication.class, args);
    }
}
