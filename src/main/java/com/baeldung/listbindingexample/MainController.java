package com.baeldung.listbindingexample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
    public MainController() {
    }

    @GetMapping(value = "/")
    public String root() {
        return "redirect:index";
    }

    @GetMapping(value = "/index")
    public String index(Model model) {
        model.addAttribute("name", "denys");
        return "index";
    }

    @GetMapping(value = "/ping")
    public String ping() {
        return "pong";
    }
}
