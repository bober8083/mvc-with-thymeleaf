package com.baeldung.listbindingexample;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class InMemoryBookService implements BookService {
    private final AtomicLong bookCounter = new AtomicLong();
    private final BookRepository bookRepository;

    public InMemoryBookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
        long maxId = detectMaxId();
        bookCounter.set(maxId);
    }

    private long detectMaxId() {
        List<Book> books = this.bookRepository.findAll();
        if (CollectionUtils.isEmpty(books)) {
            return 1;
        }
        Optional<Book> res = books.stream().max((b1, b2) -> {
            return b1.id < b2.id ? -1 : 1;
        });
        if (res.isPresent()) {
            return res.get().id;
        }
        return 1;
    }

    @Override
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    public void saveAll(List<Book> books) {
        long nextId = getNextId();
        for (Book book : books) {
            if (book.id == 0) {
                book.id = nextId++;
            }
        }

        Map<Long, Book> bookMap = books.stream()
            .collect(Collectors.toMap(Book::getId, Function.identity()));

        bookRepository.saveAll(bookMap.values());
    }

    private Long getNextId() {
        return bookCounter.incrementAndGet();
    }
}
